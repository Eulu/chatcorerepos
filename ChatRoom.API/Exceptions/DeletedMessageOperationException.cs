﻿using System;

namespace ChatRoom.API.Exceptions
{
    public class DeletedMessageOperationException : Exception
    {
        public DeletedMessageOperationException() : base()
        {
        }

        public DeletedMessageOperationException(string message)
            : base(message)
        {
        }

        public DeletedMessageOperationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
