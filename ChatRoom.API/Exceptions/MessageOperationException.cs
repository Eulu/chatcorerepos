﻿using System;

namespace ChatRoom.API.Exceptions
{
    public class MessageOperationException : Exception
    {
        public MessageOperationException() : base()
        {
        }

        public MessageOperationException(string message)
            : base(message)
        {
        }

        public MessageOperationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
