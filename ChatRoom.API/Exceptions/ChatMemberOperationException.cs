﻿using System;

namespace ChatRoom.API.Exceptions
{
    class ChatMemberOperationException : Exception
    {
        public ChatMemberOperationException() : base()
        {
        }

        public ChatMemberOperationException(string message)
            : base(message)
        {
        }

        public ChatMemberOperationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
