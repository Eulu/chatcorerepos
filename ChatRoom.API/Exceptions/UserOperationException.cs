﻿using System;

namespace ChatRoom.API.Exceptions
{
    public class UserOperationException : Exception
    {
        public UserOperationException() : base()
        {
        }

        public UserOperationException(string message)
            : base(message)
        {
        }

        public UserOperationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
