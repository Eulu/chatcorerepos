﻿using AutoMapper;
using ChatRoom.API.Models;
using ChatRoom.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ChatRoom.API.Extensions.Mappings
{
    public static class ChatMappingExtension
    {
        public static Chat ToChat(this ChatModel chatModel)
        {
            return Mapper.Map<ChatModel, Chat>(chatModel);
        }

        public static ChatModel ToChatModel(this Chat user)
        {
            return Mapper.Map<Chat, ChatModel>(user);
        }

        public static List<ChatModel> ToChatModelList(this List<Chat> chats)
        {
            return chats?.Select(p => p.ToChatModel()).ToList();
        }
    }
}
