﻿using AutoMapper;
using ChatRoom.API.Models;
using ChatRoom.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ChatRoom.API.Extensions.Mappings
{
    public static class DeletedMessageMappingExtension
    {
        public static DeletedMessage ToDeletedMessage(this DeletedMessageModel deletedMessageModel)
        {
            return Mapper.Map<DeletedMessageModel, DeletedMessage>(deletedMessageModel);
        }

        public static DeletedMessageModel ToDeletedMessageModel(this DeletedMessage deletedMessage)
        {
            return Mapper.Map<DeletedMessage, DeletedMessageModel>(deletedMessage);
        }

        public static List<DeletedMessageModel> ToDeletedMessageModellList(this List<DeletedMessage> deletedMessages)
        {
            return deletedMessages?.Select(p => p.ToDeletedMessageModel()).ToList();
        }
    }
}
