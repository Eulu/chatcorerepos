﻿using AutoMapper;
using ChatRoom.API.Models;
using ChatRoom.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ChatRoom.API.Extensions.Mappings
{
    public static class MessageMappingExtension
    {
        public static Message ToMessage(this MessageModel messageModel)
        {
            return Mapper.Map<MessageModel, Message>(messageModel);
        }

        public static MessageModel ToMessageModel(this Message message)
        {
            return Mapper.Map<Message, MessageModel>(message);
        }

        public static List<MessageModel> ToMessageModelList(this List<Message> message)
        {
            return message?.Select(p => p.ToMessageModel()).ToList();
        }
    }
}
