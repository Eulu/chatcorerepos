﻿using AutoMapper;
using ChatRoom.API.Models;
using ChatRoom.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ChatRoom.API.Extensions.Mappings
{
    public static class ChatMembersMappingExtension
    {
        public static ChatMember ToChatMembers(this ChatMemberModel userModel)
        {
            return Mapper.Map<ChatMemberModel, ChatMember>(userModel);
        }

        public static ChatMemberModel ToChatMemberModel(this ChatMember user)
        {
            return Mapper.Map<ChatMember, ChatMemberModel>(user);
        }

        public static List<ChatMemberModel> ToChatMemberModelList(this List<ChatMember> chatMembers)
        {
            return chatMembers?.Select(p => p.ToChatMemberModel()).ToList();
        }
    }
}
