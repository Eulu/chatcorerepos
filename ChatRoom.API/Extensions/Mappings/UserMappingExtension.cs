﻿using AutoMapper;
using ChatRoom.API.Models;
using ChatRoom.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ChatRoom.API.Extensions.Mappings
{
    public static class UserMappingExtension
    {
        public static User ToUser(this UserModel userModel)
        {
            return Mapper.Map<UserModel, User>(userModel);
        }

        public static UserModel ToUserModel(this User user)
        {
            return Mapper.Map<User, UserModel>(user);
        }

        public static List<UserModel> ToUserModelList(this List<User> users)
        {
            return users?.Select(p => p.ToUserModel()).ToList();
        }

        public static List<User> ToUserList(this List<UserModel> userModels)
        {
            return userModels?.Select(p => p.ToUser()).ToList();
        }
    }
}
