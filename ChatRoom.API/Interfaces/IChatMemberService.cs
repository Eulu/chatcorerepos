﻿using ChatRoom.API.Models;
using ChatRoom.API.Services;
using System.Threading.Tasks;

namespace ChatRoom.API.Interfaces
{
    public interface IChatMemberService
    {
        Task<OperationResult<string>> Add(ChatMemberModel chatMemberModel);
        Task<OperationResult<ChatMemberModel>> GetAll();
    }
}
