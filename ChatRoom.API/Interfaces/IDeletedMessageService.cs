﻿using ChatRoom.API.Models;
using ChatRoom.API.Services;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Interfaces
{
    public interface IDeletedMessageService
    {
        Task<OperationResult<string>> Add(DeletedMessageModel deletedMessageModel);
        Task<OperationResult<DeletedMessageModel>> Get(Guid userId);
        Task<OperationResult<DeletedMessageModel>> GetAll();    
    }
}
