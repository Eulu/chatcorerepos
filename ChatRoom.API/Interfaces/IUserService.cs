﻿using ChatRoom.API.Models;
using ChatRoom.API.Services;
using ChatRoom.DAL.Entities;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Interfaces
{
    public interface IUserService
    {
        Task<OperationResult<string>> Add(UserModel userModel);

        Task<OperationResult<string>> Remove(Guid id);

        Task<OperationResult<string>> Update(UserModel userModel);

        Task<OperationResult<UserModel>> GetAll();

        Task<OperationResult<UserModel>> Get(Guid id);
    }
}
