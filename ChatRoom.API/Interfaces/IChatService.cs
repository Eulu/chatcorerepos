﻿using ChatRoom.API.Models;
using ChatRoom.API.Services;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Interfaces
{
    public interface IChatService
    {
        Task<OperationResult<string>> Add(ChatModel chatModel);

        Task<OperationResult<string>> Remove(Guid id);

        Task<OperationResult<string>> Update(ChatModel chatModel);

        Task<OperationResult<ChatModel>> GetAll();

        Task<OperationResult<ChatModel>> Get(Guid id);
    }
}
