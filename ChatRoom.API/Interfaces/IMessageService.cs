﻿using ChatRoom.API.Models;
using ChatRoom.API.Services;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Interfaces
{
    public interface IMessageService
    {
        Task<OperationResult<string>> Add(MessageModel message);

        Task<OperationResult<string>> Remove(Guid id);

        Task<OperationResult<string>> Update(MessageModel message);

        Task<OperationResult<MessageModel>> GetAll(Guid userId);

        Task<OperationResult<MessageModel>> GetMessagesByPage(int page);

        Task<OperationResult<MessageModel>> Get(Guid id);
    }
}
