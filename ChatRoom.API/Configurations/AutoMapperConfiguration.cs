﻿using AutoMapper;
using ChatRoom.API.Models;
using ChatRoom.DAL.Entities;

namespace ChatRoom.API.Configurations
{
    public class AutoMapperConfigurationProfile : Profile
    {
        public AutoMapperConfigurationProfile()
        {
            CreateMap<Message, MessageModel>();
            CreateMap<MessageModel, Message>();

            CreateMap<ChatMember, ChatMemberModel>();
            CreateMap<ChatMemberModel, ChatMember>();

            CreateMap<DeletedMessage, DeletedMessageModel>();
            CreateMap<DeletedMessageModel, DeletedMessage>();

            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>();

            CreateMap<Chat, ChatModel>();
            CreateMap<ChatModel, Chat>();

        }
    }
}
