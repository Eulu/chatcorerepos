﻿using System;

namespace ChatRoom.API.Models
{
    public class ReplyModel
    {
        public MessageModel messageModel { get; set; }
        public Guid messageID { get; set; }
        public bool ReplyToMessageOwner { get; set; }
    }
}
