﻿using System;

namespace ChatRoom.API.Models
{
    public class DeletedMessageModel
    {
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public Guid MessageID { get; set; }
    }
}
