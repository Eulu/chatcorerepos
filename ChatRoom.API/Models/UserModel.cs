﻿using System;

namespace ChatRoom.API.Models
{
    public class UserModel
    {
        public Guid ID { get; set; } = Guid.NewGuid();
        public string UserName{ get; set; }
    }
}
