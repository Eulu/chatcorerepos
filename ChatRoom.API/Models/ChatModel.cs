﻿using System;

namespace ChatRoom.API.Models
{
    public class ChatModel
    {
        public Guid ID { get; set; }
        public String Name { get; set; }
    }
}
