﻿using System;

namespace ChatRoom.API.Models
{
    public class DeleteModel
    {
        public Guid messageID { get; set; }
        public bool DeleteForEverybody { get; set; }
    }
}
