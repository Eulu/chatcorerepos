﻿using System;

namespace ChatRoom.API.Models
{
    public class ChatMemberModel
    {
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
    }
}
