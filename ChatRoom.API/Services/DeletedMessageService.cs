﻿using ChatRoom.API.Exceptions;
using ChatRoom.API.Extensions.Mappings;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Services
{
    public class DeletedMessageService : IDeletedMessageService
    {
        private readonly IDeletedMessageRepository _deletedMessageRepository;
        public DeletedMessageService(IDeletedMessageRepository deletedMessageRepository)
        {
            _deletedMessageRepository = deletedMessageRepository;
        }

        public async Task<OperationResult<string>> Add(DeletedMessageModel deletedMessageModel)
        {
            try
            {
                await _deletedMessageRepository.Add(deletedMessageModel.ToDeletedMessage());
                return new OperationResult<string>()
                {
                    Result = $"DeletedMessage with id: {deletedMessageModel.MessageID} was added sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while adding a DeletedMessage {ex.Message}");

                return new OperationResult<string>()
                {
                    Error = $"DeletedMessage with id: {deletedMessageModel.ID} was not added",
                    IsSuccess = false,
                };

                throw new DeletedMessageOperationException("Oops smth went wrong while adding a DeletedMessage");
            }
        }

        public async Task<OperationResult<DeletedMessageModel>> Get(Guid userId)
        {
            try
            {
                var deletedMessages = await _deletedMessageRepository.Get(userId);

                return new OperationResult<DeletedMessageModel>()
                {
                    IsSuccess = true,
                    Results = deletedMessages.ToDeletedMessageModellList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while retriving a DeletedMessage {ex}");

                return new OperationResult<DeletedMessageModel>()
                {
                    Error = $"DeletedMessage with id: {userId} not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<DeletedMessageModel>> GetAll()
        {
            try
            {
                var deletedMessages = await _deletedMessageRepository.GetAll();

                return new OperationResult<DeletedMessageModel>()
                {
                    IsSuccess = true,
                    Results = deletedMessages.ToDeletedMessageModellList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while loading all DeletedMessages {ex}");

                return new OperationResult<DeletedMessageModel>()
                {
                    Error = $"DeletedMessage for were not found",
                    IsSuccess = false,
                };
            }
        }
    }
}
