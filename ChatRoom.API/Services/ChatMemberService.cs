﻿using ChatRoom.API.Exceptions;
using ChatRoom.API.Extensions.Mappings;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Services
{
    public class ChatMemberService : IChatMemberService
    {
        private readonly IChatMemberRepository _chatMemberRepository;
        public ChatMemberService(IChatMemberRepository chatMemberRepository)
        {
            _chatMemberRepository = chatMemberRepository;
        }

        public async Task<OperationResult<string>> Add(ChatMemberModel chatMemberModel)
        {
            try
            {
                await _chatMemberRepository.Add(chatMemberModel.ToChatMembers());
                return new OperationResult<string>()
                {
                    Result = $"ChatMember with id: {chatMemberModel.ID} was added sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while adding a chatMember {ex.Message}");

                return new OperationResult<string>()
                {
                    Error = $"ChatMember with id: {chatMemberModel.ID} was not added",
                    IsSuccess = false,
                };

                throw new ChatMemberOperationException("Oops smth went wrong while adding a chatMember");
            }
        }

        public async Task<OperationResult<ChatMemberModel>> GetAll()
        {
            try
            {
                var chatMembers = await _chatMemberRepository.GetAll();

                return new OperationResult<ChatMemberModel>()
                {
                    IsSuccess = true,
                    Results = chatMembers.ToChatMemberModelList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while loading all ChatMembers {ex}");

                return new OperationResult<ChatMemberModel>()
                {
                    Error = $"ChatMembers were not found",
                    IsSuccess = false,
                };
            }
        }
    }
}
