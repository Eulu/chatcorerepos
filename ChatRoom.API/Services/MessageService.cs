﻿using ChatRoom.API.Exceptions;
using ChatRoom.API.Extensions.Mappings;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;
        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public async Task<OperationResult<string>> Add(MessageModel messageModel)
        {
            try
            {
                await _messageRepository.Add(messageModel.ToMessage());
                return new OperationResult<string>()
                {
                    Result = $"Message with id: {messageModel.ID} was added sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while adding a Message {ex.Message}");

                return new OperationResult<string>()
                {
                    Error = $"Message with id: {messageModel.ID} was not added",
                    IsSuccess = false,
                };

                throw new MessageOperationException("Oops smth went wrong while adding a Message");
            }
        }

        public async Task<OperationResult<MessageModel>> Get(Guid id)
        {
            try
            {
                var operationResult = new OperationResult<MessageModel>();
                var messages = await _messageRepository.Get(id);

                return new OperationResult<MessageModel>()
                {
                    Result = messages.ToMessageModel(),
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while retriving a Message {ex}");

                return new OperationResult<MessageModel>()
                {
                    Error = $"Message with id: {id} not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<MessageModel>> GetAll(Guid userId)
        {
            try
            {
                var messages = await _messageRepository.GetAll(userId);

                return new OperationResult<MessageModel>()
                {
                    IsSuccess = true,
                    Results = messages.ToMessageModelList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while loading all Message {ex}");

                return new OperationResult<MessageModel>()
                {
                    Error = $"Messages for {userId} were not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<MessageModel>> GetMessagesByPage(int page)
        {
            try
            {
                var messages = await _messageRepository.GetMessagesByPage(page);

                return new OperationResult<MessageModel>()
                {
                    IsSuccess = true,
                    Results = messages.ToMessageModelList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while loading Messages for page {ex}");

                return new OperationResult<MessageModel>()
                {
                    Error = $"Messages for page {page} were not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<string>> Remove(Guid id)
        {
            try
            {
                await _messageRepository.Remove(id);
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while deleting a Message {ex.Message}");

                return new OperationResult<string>()
                {
                    Error = $"Message with id: {id} not found",
                    IsSuccess = false,
                };
            }

            return new OperationResult<string>()
            {
                Result = $"Message with id: {id} was removed sucsessfully",
                IsSuccess = true
            };
        }

        public async Task<OperationResult<string>> Update(MessageModel message)
        {
            try
            {
                await _messageRepository.Update(message.ToMessage());
                return new OperationResult<string>()
                {
                    Result = $"Message with id: {message.ID} was updated sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while updating a Message {ex}");

                return new OperationResult<string>()
                {
                    Error = $"Message with id: {message.ID} was not updated",
                    IsSuccess = false,
                };
            }


        }
    }
}
