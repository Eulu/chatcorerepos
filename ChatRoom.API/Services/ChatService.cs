﻿using ChatRoom.API.Extensions.Mappings;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Services
{
    public class ChatService : IChatService
    {
        private readonly IChatRepository _chatRepository;
        public ChatService(IChatRepository chatRepository)
        {
            _chatRepository = chatRepository;
        }

        public async Task<OperationResult<string>> Add(ChatModel chatModel)
        {
            try
            {
                await _chatRepository.Add(chatModel.ToChat());
                return new OperationResult<string>()
                {
                    Result = $"ChatModel with id: {chatModel.ID} was added sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while adding a ChatModel {ex.ChatModel}");

                return new OperationResult<string>()
                {
                    Error = $"ChatModel with id: {chatModel.ID} was not added",
                    IsSuccess = false,
                };

                //throw new ChatOperationException("Oops smth went wrong while adding a ChatModel");
            }
        }
        public async Task<OperationResult<ChatModel>> Get(Guid id)
        {
            try
            {
                var operationResult = new OperationResult<ChatModel>();
                var chats = await _chatRepository.Get(id);

                return new OperationResult<ChatModel>()
                {
                    Result = chats.ToChatModel(),
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while retriving a Chat {ex}");

                return new OperationResult<ChatModel>()
                {
                    Error = $"Message with id: {id} not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<ChatModel>> GetAll()
        {
            try
            {
                var chats = await _chatRepository.GetAll();

                return new OperationResult<ChatModel>()
                {
                    IsSuccess = true,
                    Results = chats.ToChatModelList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while loading all Chats {ex}");

                return new OperationResult<ChatModel>()
                {
                    Error = $"Chats were not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<string>> Remove(Guid id)
        {
            try
            {
                await _chatRepository.Remove(id);
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while deleting a ChatModel {ex.ChatModel}");

                return new OperationResult<string>()
                {
                    Error = $"Message with id: {id} not found",
                    IsSuccess = false,
                };
            }

            return new OperationResult<string>()
            {
                Result = $"Message with id: {id} was removed sucsessfully",
                IsSuccess = true
            };
        }

        public async Task<OperationResult<string>> Update(ChatModel chatModel)
        {
            try
            {
                await _chatRepository.Update(chatModel.ToChat());
                return new OperationResult<string>()
                {
                    Result = $"ChatModel with id: {chatModel.ID} was updated sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while updating a ChatModel {ex}");

                return new OperationResult<string>()
                {
                    Error = $"ChatModel with id: {chatModel.ID} was not updated",
                    IsSuccess = false,
                };
            }
        }
    }
}
