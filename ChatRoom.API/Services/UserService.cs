﻿using ChatRoom.API.Exceptions;
using ChatRoom.API.Extensions.Mappings;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace ChatRoom.API.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<OperationResult<string>> Add(UserModel userModel)
        {
            try
            {
                await _userRepository.Add(userModel.ToUser());
                return new OperationResult<string>()
                {
                    Result = $"User with id: {userModel.ID} was added sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while adding a User {ex.Message}");

                return new OperationResult<string>()
                {
                    Error = $"User with id: {userModel.ID} was not added",
                    IsSuccess = false,
                };

                throw new MessageOperationException("Oops smth went wrong while adding a User");
            }
        }

        public async Task<OperationResult<UserModel>> Get(Guid id)
        {
            try
            {
                var operationResult = new OperationResult<UserModel>();
                var user = await _userRepository.Get(id);

                return new OperationResult<UserModel>()
                {
                    Result = user.ToUserModel(),
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while retriving a User {ex}");

                return new OperationResult<UserModel>()
                {
                    Error = $"User with id: {id} not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<UserModel>> GetAll()
        {
            try
            {
                var users = await _userRepository.GetAll();

                return new OperationResult<UserModel>()
                {
                    IsSuccess = true,
                    Results = users.ToUserModelList()
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while loading all Users {ex}");

                return new OperationResult<UserModel>()
                {
                    Error = $"Users were not found",
                    IsSuccess = false,
                };
            }
        }

        public async Task<OperationResult<string>> Remove(Guid id)
        {
            try
            {
                await _userRepository.Remove(id);
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while deleting a User {ex.Message}");

                return new OperationResult<string>()
                {
                    Error = $"User with id: {id} not found",
                    IsSuccess = false,
                };
            }

            return new OperationResult<string>()
            {
                Result = $"User with id: {id} was removed sucsessfully",
                IsSuccess = true
            };
        }

        public async Task<OperationResult<string>> Update(UserModel userModel)
        {
            try
            {
                await _userRepository.Update(userModel.ToUser());
                return new OperationResult<string>()
                {
                    Result = $"User with id: {userModel.ID} was updated sucsessfully",
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                //_logger.Error($"An Error occured while updating a User {ex}");

                return new OperationResult<string>()
                {
                    Error = $"User with id: {userModel.ID} was not updated",
                    IsSuccess = false,
                };
            }
        }
    }
}
