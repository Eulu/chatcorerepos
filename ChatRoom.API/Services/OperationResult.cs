﻿using System.Collections.Generic;

namespace ChatRoom.API.Services
{
    public class OperationResult<T>
    {
        public string Error { get; set; }

        public bool IsSuccess { get; set; }

        public T Result { get; set; }

        public List<T> Results { get; set; }
    }
}
