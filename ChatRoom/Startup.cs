﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ChatRoom.DAL;
using AutoMapper;
using ChatRoom.API.Configurations;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Services;
using ChatRoom.DAL.Interfaces;
using ChatRoom.DAL.Repositories;
using Serilog;

namespace ChatRoom
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
            Configuration.GetConnectionString("DefaultConnection")));
            ConfigureIoc(services);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            ConfigureAutoMapper();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
        public void ConfigureIoc(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IMessageRepository, MessageRepository>();

            services.AddTransient<IChatMemberService, ChatMemberService>();
            services.AddTransient<IChatMemberRepository, ChatMemberRepository>();

            services.AddTransient<IDeletedMessageService, DeletedMessageService>();
            services.AddTransient<IDeletedMessageRepository, DeletedMessageRepository>();

            services.AddTransient<IChatService, ChatService>();
            services.AddTransient<IChatRepository, ChatRepository>();

            services.AddSingleton((ILogger)new LoggerConfiguration()
               .ReadFrom.Configuration(Configuration)
               .CreateLogger());
        }
        public void ConfigureAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperConfigurationProfile>();
            });
        }
    }
}
