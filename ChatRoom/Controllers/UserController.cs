﻿using System;
using System.Threading.Tasks;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace ChatRoom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        public async Task<OperationResult<string>> Add([FromBody] UserModel userModel)
        {
            var result = await _userService.Add(userModel);

            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("Board was not created");
                return result;
            }
        }

        [HttpDelete("{id}")]
        public async Task<OperationResult<string>> Delete(Guid id)
        {
            var result = await _userService.Remove(id);
            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("Board was not removed");
                return result;
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UserModel userModel)
        {
            var result = await _userService.Update(userModel);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Board was not updated");
                return BadRequest(result.Error);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _userService.Get(id);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving a board");
                return BadRequest(result.Error);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Users()
        {
            var result = await _userService.GetAll();
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving all boards");
                return BadRequest(result.Error);
            }
        }
    }
}