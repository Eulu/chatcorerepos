﻿using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ChatRoom.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;
        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpPost]
        public async Task<OperationResult<string>> Add([FromBody] MessageModel messageModel)
        {
            var result = await _messageService.Add(messageModel);

            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("MessageModel was not created");
                return result;
            }
        }
        [HttpPost("reply")]
        public async Task<OperationResult<string>> Reply([FromBody] ReplyModel replyModel)
        {
            var ParentMessage = _messageService.Get(replyModel.messageID);
            if (replyModel.ReplyToMessageOwner)
            {
                replyModel.messageModel.DestinationID = ParentMessage.Result.Result.MessageOwnerID;
            }
            else
            {
                replyModel.messageModel.DestinationID = ParentMessage.Result.Result.DestinationID; ;
            }
            replyModel.messageModel.RepliedMessageID = replyModel.messageID;
            replyModel.messageModel.isReplied = true;
            var result = await _messageService.Add(replyModel.messageModel);

            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("Message was not replyied");
                return result;
            }
        }

        [HttpDelete("{id}")]
        public async Task<OperationResult<string>> Delete(Guid id)
        {
            var result = await _messageService.Remove(id);
            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("MessageModel was not removed");
                return result;
            }
        }
       

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] MessageModel messageModel)
        {
            messageModel.isEdited = true;
            var result = await _messageService.Update(messageModel);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("MessageModel was not updated");
                return BadRequest(result.Error);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _messageService.Get(id);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving a MessageModel");
                return BadRequest(result.Error);
            }
        }

        [HttpGet("messagesByUser/{userId}")]
        public async Task<IActionResult> GetMessagesByUserId(Guid userId)
        {
            var result = await _messageService.GetAll(userId);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving all MessageModels");
                return BadRequest(result.Error);
            }
        }

        [HttpGet("messagesByPage/{page}")]
        public async Task<IActionResult> GetMessagesByPage(int page)
        {
            var result = await _messageService.GetMessagesByPage(page);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving all MessageModels");
                return BadRequest(result.Error);
            }
        }
    }
}