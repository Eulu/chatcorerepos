﻿using System.Threading.Tasks;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace ChatRoom.UI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatMemberController : ControllerBase
    {
        private readonly IChatMemberService _chatMemberService;
        private Serilog.ILogger _logger;
        public ChatMemberController(IChatMemberService chatMemberService, Serilog.ILogger logger)
        {
            _chatMemberService = chatMemberService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<OperationResult<string>> Add([FromBody] ChatMemberModel chatMemberModel)
        {
            var result = await _chatMemberService.Add(chatMemberModel);

            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("ChatMember was not created");
                return result;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            _logger.Error("Test1"); 
            var result = await _chatMemberService.GetAll();
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving all ChatMembers");
                return BadRequest(result.Error);
            }
        }
    }
}