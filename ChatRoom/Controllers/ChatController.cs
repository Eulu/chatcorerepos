﻿using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ChatRoom.UI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IChatService _chatService;
        private Serilog.ILogger _logger;
        public ChatController(IChatService chatService, Serilog.ILogger logger)
        {
            _chatService = chatService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<OperationResult<string>> Add([FromBody]ChatModel chatModel)
        {
            var result = await _chatService.Add(chatModel);

            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                _logger.Error("ChatModel was not created");
                return result;
            }
        }

        [HttpDelete("{id}")]
        public async Task<OperationResult<string>> Delete(Guid id)
        {
            var result = await _chatService.Remove(id);
            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                _logger.Error("ChatModel was not removed");
                return result;
            }
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ChatModel chatModel)
        {
            
            var result = await _chatService.Update(chatModel);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                _logger.Error("ChatModel was not updated");
                return BadRequest(result.Error);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _chatService.Get(id);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                _logger.Error("Error retriving a ChatModel");
                return BadRequest(result.Error);
            }
        }


        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _chatService.GetAll();
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                _logger.Error("Error retriving all ChatModels");
                return BadRequest(result.Error);
            }
        }
    }
}