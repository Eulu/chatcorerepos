﻿using System;
using System.Threading.Tasks;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace ChatRoom.UI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeletedMessageController : ControllerBase
    {
        private readonly IDeletedMessageService _deletedMessageService;
        public DeletedMessageController(IDeletedMessageService deletedMessageService)
        {
            _deletedMessageService = deletedMessageService;
        }

        [HttpPost]
        public async Task<OperationResult<string>> Add([FromBody] DeletedMessageModel deletedMessageModel)
        {
            var result = await _deletedMessageService.Add(deletedMessageModel);

            if (result.IsSuccess)
            {
                return result;
            }
            else
            {
                //_logger.Error("Board was not created");
                return result;
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _deletedMessageService.Get(id);
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving a board");
                return BadRequest(result.Error);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Users()
        {
            var result = await _deletedMessageService.GetAll();
            if (result.IsSuccess)
            {
                return Ok(result);
            }
            else
            {
                //_logger.Error("Error retriving all boards");
                return BadRequest(result.Error);
            }
        }

    }
}