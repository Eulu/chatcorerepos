using ChatRoom.API.Extensions.Mappings;
using ChatRoom.API.Interfaces;
using ChatRoom.API.Models;
using ChatRoom.Controllers;
using ChatRoom.DAL.Entities;
using GenFu;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ChatRoom.Tests
{
    public class UserControllerTest
    {
        private IEnumerable<UserModel> GetFakeData()
        {
            IEnumerable<UserModel> persons = new List<UserModel>(){ 
                new UserModel { ID = new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71"), UserName = "���", },
                new UserModel { ID = new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), UserName = "�����", },
                new UserModel { ID = new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3"), UserName = "�����", },
                new UserModel { ID = new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec"), UserName = "����", },
                new UserModel { ID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), UserName = "�������" } 
            };
           
            return persons.Select(_ => _);
        }

        [Fact]
        public void GetUserTest()
        {
            // arrange
            var service = new Mock<IUserService>();
            var persons = GetFakeData();
            //var dataFromService = service.Setup(x => x.GetAll());
           // service.Setup(x => x.GetAll()).Returns(Task.CompletedTask);
            var controller = new UserController(service.Object);
            // Act
            var results = controller.Users().ToAsyncEnumerable();
            var count = results.Count();
            // Assert
            Assert.Equal(count.Result, 5);
        }
    }
}
