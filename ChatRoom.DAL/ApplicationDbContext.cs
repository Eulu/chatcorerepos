﻿using ChatRoom.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ChatRoom.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<ChatMember> ChatsMembers { get; set; }

        public DbSet<Chat> Chats { get; set; }

        public DbSet<DeletedMessage> DeletedMessages { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User { ID = new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71"), UserName = "Юра", },
                new User { ID = new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), UserName = "Бабця", },
                new User { ID = new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3"), UserName = "Настя", },
                new User { ID = new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec"), UserName = "Саша", },
                new User { ID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), UserName = "Михайло" }
                );

            modelBuilder.Entity<ChatMember>().HasData(
                new ChatMember { ID = Guid.NewGuid(), UserID = new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71") },
                new ChatMember { ID = Guid.NewGuid(), UserID = new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538") },
                new ChatMember { ID = Guid.NewGuid(), UserID = new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3") },
                new ChatMember { ID = Guid.NewGuid(), UserID = new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec") },
                new ChatMember { ID = Guid.NewGuid(), UserID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6") }
                );
            modelBuilder.Entity<Chat>().HasData(
                new Chat { ID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), Name = "Global" }
                );

           modelBuilder.Entity<Message>().HasData(
                new Message { ID = new Guid("07d6e226-c96b-4842-86ff-60d81bdc56ee"), MessageOwnerID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), DestinationID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), RepliedMessageID = null, Text = "Всім привіт!", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:03:06") },
                new Message { ID = new Guid("cf714c86-c719-4a92-babe-760807661216"), MessageOwnerID = new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71"), DestinationID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), RepliedMessageID = null, Text = "Привіт! Що ти там?", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:04:12") },
                new Message { ID = new Guid("ee86453e-bf4a-4f76-89c2-12870380aa13"), MessageOwnerID = new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), DestinationID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), RepliedMessageID = null, Text = "Коли вже карантин закінчиться?", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:04:32") },
                new Message { ID = new Guid("539a779b-2ca0-4022-bea3-8abab95ba39c"), MessageOwnerID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), DestinationID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), RepliedMessageID = null, Text = "Жди! Закінчиться...", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:05:20 ") },
                new Message { ID = new Guid("10ebc7ef-bed5-40a2-a2b6-f74a43411bb8"), MessageOwnerID = new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), DestinationID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), RepliedMessageID = null, Text = "Хочу на природу вже", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:05:53") },
                new Message { ID = new Guid("0f791ce8-8247-4c18-99f8-22d9ddafde51"), MessageOwnerID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), DestinationID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), RepliedMessageID = null, Text = "Давайте в доту?", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:06:01") },
                new Message { ID = new Guid("e92ae190-2b06-4ce8-a336-7f4951789abf"), MessageOwnerID = new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), DestinationID = new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), RepliedMessageID = null, Text = "го))0)", isEdited = false, isReplied = false, DateTime = DateTime.Parse("15/06/2020 10:06:11") }
                );

        }
    }
}
