﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatRoom.DAL.Migrations
{
    public partial class AddedLogger2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("0e2bf5a3-bc2d-4e5e-b71f-7a0389153ac1"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("22277b1b-9684-4001-9f9b-1b3b4cc02082"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("2ea3d17e-2b7f-4b87-bea7-d9509d47d948"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("2ec26b20-3f97-4829-b5da-97a50472ced2"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("ede259f9-7d86-4471-8614-75beb0fe665c"));

            migrationBuilder.InsertData(
                table: "ChatsMembers",
                columns: new[] { "ID", "UserID" },
                values: new object[,]
                {
                    { new Guid("a2949e24-1bc4-492a-a204-0dadd0aa6c82"), new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71") },
                    { new Guid("8d5e9dc7-1298-49c0-8ca6-a3898465b3ef"), new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538") },
                    { new Guid("bfeeba59-f3b7-488f-b15a-f28f9fceb766"), new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3") },
                    { new Guid("f5964615-e96f-4c1f-a475-5e882e3e8e32"), new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec") },
                    { new Guid("fda9212e-6df3-4827-9686-2873765ada76"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("8d5e9dc7-1298-49c0-8ca6-a3898465b3ef"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("a2949e24-1bc4-492a-a204-0dadd0aa6c82"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("bfeeba59-f3b7-488f-b15a-f28f9fceb766"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("f5964615-e96f-4c1f-a475-5e882e3e8e32"));

            migrationBuilder.DeleteData(
                table: "ChatsMembers",
                keyColumn: "ID",
                keyValue: new Guid("fda9212e-6df3-4827-9686-2873765ada76"));

            migrationBuilder.InsertData(
                table: "ChatsMembers",
                columns: new[] { "ID", "UserID" },
                values: new object[,]
                {
                    { new Guid("0e2bf5a3-bc2d-4e5e-b71f-7a0389153ac1"), new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71") },
                    { new Guid("22277b1b-9684-4001-9f9b-1b3b4cc02082"), new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538") },
                    { new Guid("2ea3d17e-2b7f-4b87-bea7-d9509d47d948"), new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3") },
                    { new Guid("2ec26b20-3f97-4829-b5da-97a50472ced2"), new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec") },
                    { new Guid("ede259f9-7d86-4471-8614-75beb0fe665c"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6") }
                });
        }
    }
}
