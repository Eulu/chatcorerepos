﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatRoom.DAL.Migrations
{
    public partial class CreateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Chats",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chats", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ChatsMembers",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatsMembers", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DeletedMessages",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserID = table.Column<Guid>(nullable: false),
                    MessageID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeletedMessages", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MessageOwnerID = table.Column<Guid>(nullable: false),
                    DestinationID = table.Column<Guid>(nullable: false),
                    RepliedMessageID = table.Column<Guid>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    isEdited = table.Column<bool>(nullable: false),
                    isReplied = table.Column<bool>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.InsertData(
                table: "Chats",
                columns: new[] { "ID", "Name" },
                values: new object[] { new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), "Global" });

            migrationBuilder.InsertData(
                table: "ChatsMembers",
                columns: new[] { "ID", "UserID" },
                values: new object[,]
                {
                    { new Guid("75489b19-56a8-4e38-8009-2071f15d2df7"), new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71") },
                    { new Guid("cda3079f-de01-43b7-bd0d-02569cecf35a"), new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538") },
                    { new Guid("1c3b538d-2330-43ce-8470-e4143d746b0b"), new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3") },
                    { new Guid("52fecbe1-4c12-4fd3-8971-2fe6f2237963"), new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec") },
                    { new Guid("b394be66-53a2-4a99-be7d-0fc9f8b02f57"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6") }
                });

            migrationBuilder.InsertData(
                table: "Messages",
                columns: new[] { "ID", "DateTime", "DestinationID", "MessageOwnerID", "RepliedMessageID", "Text", "isEdited", "isReplied" },
                values: new object[,]
                {
                    { new Guid("e92ae190-2b06-4ce8-a336-7f4951789abf"), new DateTime(2020, 6, 15, 10, 6, 11, 0, DateTimeKind.Unspecified), new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), null, "Дев'ять?)))))", false, false },
                    { new Guid("0f791ce8-8247-4c18-99f8-22d9ddafde51"), new DateTime(2020, 6, 15, 10, 6, 1, 0, DateTimeKind.Unspecified), new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), null, "10:06, бабуль", false, false },
                    { new Guid("10ebc7ef-bed5-40a2-a2b6-f74a43411bb8"), new DateTime(2020, 6, 15, 10, 5, 53, 0, DateTimeKind.Unspecified), new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), null, "Міша, я ж тільки не чую... Читаю то добре!", false, false },
                    { new Guid("ee86453e-bf4a-4f76-89c2-12870380aa13"), new DateTime(2020, 6, 15, 10, 4, 32, 0, DateTimeKind.Unspecified), new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), null, "Скажіть скільки годин...", false, false },
                    { new Guid("cf714c86-c719-4a92-babe-760807661216"), new DateTime(2020, 6, 15, 10, 4, 12, 0, DateTimeKind.Unspecified), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71"), null, "Привіт! Що ти там?", false, false },
                    { new Guid("07d6e226-c96b-4842-86ff-60d81bdc56ee"), new DateTime(2020, 6, 15, 10, 3, 6, 0, DateTimeKind.Unspecified), new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), null, "Всім привіт!", false, false },
                    { new Guid("539a779b-2ca0-4022-bea3-8abab95ba39c"), new DateTime(2020, 6, 15, 10, 5, 20, 0, DateTimeKind.Unspecified), new Guid("becb3b8d-3045-41a1-82ef-61ea6a3a577e"), new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), null, "Баб, тільки не це...", false, false }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "ID", "UserName" },
                values: new object[,]
                {
                    { new Guid("f2d989f2-a9f5-4513-ac09-4410e016ffec"), "Саша" },
                    { new Guid("3f4bba8b-ca6f-4b32-9909-8df0f9862e71"), "Юра" },
                    { new Guid("3fe0ee37-59f6-4470-ac49-7ab04a4ff538"), "Бабця" },
                    { new Guid("c2bc90e2-e88d-4de1-9612-27a6646a3ec3"), "Настя" },
                    { new Guid("23a0d5b2-da52-43cc-aec9-8c708ef122c6"), "Михайло" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Chats");

            migrationBuilder.DropTable(
                name: "ChatsMembers");

            migrationBuilder.DropTable(
                name: "DeletedMessages");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
