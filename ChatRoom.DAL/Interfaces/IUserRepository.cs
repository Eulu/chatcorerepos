﻿using ChatRoom.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Interfaces
{
    public interface IUserRepository
    {
        Task Add(User user);
        Task Update(User user);
        Task Remove(Guid id);
        Task<List<User>> GetAll();
        Task<User> Get(Guid id);
    }
}
