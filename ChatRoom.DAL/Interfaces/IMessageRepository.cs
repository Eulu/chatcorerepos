﻿using ChatRoom.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Interfaces
{
    public interface IMessageRepository
    {
        Task Add(Message message);
        Task Update(Message message);
        Task Remove(Guid id);
        Task<Message> Get(Guid id);
        Task<List<Message>> GetAll(Guid userId);
        Task<List<Message>> GetMessagesByPage(int page);
    }
}
