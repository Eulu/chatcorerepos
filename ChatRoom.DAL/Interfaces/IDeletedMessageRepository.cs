﻿using ChatRoom.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Interfaces
{
    public interface IDeletedMessageRepository
    {
        Task Add(DeletedMessage deletedMessages);
        Task<List<DeletedMessage>> GetAll();
        Task<List<DeletedMessage>> Get(Guid userId);
    }
}
