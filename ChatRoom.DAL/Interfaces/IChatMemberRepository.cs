﻿using ChatRoom.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Interfaces
{
    public interface IChatMemberRepository
    {
        Task Add(ChatMember chatsMember);
        Task<List<ChatMember>> GetAll();
    }
}
