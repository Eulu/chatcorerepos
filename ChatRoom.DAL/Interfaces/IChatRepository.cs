﻿using ChatRoom.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Interfaces
{
    public interface IChatRepository
    {
        Task Add(Chat chat);
        Task Update(Chat chat);
        Task Remove(Guid id);
        Task<List<Chat>> GetAll();
        Task<Chat> Get(Guid id);

    }
}
