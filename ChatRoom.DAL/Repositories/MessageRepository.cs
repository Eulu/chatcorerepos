﻿using ChatRoom.DAL.Entities;
using ChatRoom.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly ApplicationDbContext _context;
        public MessageRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Add(Message message)
        {
            _context.Messages.Add(message);
            await _context.SaveChangesAsync();
        }

        public async Task Remove(Guid id)
        {
            _context.Messages.Remove(_context.Messages.First(x => x.ID == id));
            await _context.SaveChangesAsync();
        }

        public async Task Update(Message message)
        {
            var messageToUpdate = _context.Messages.First(b => b.ID == message.ID);
            messageToUpdate.Text = message.Text;
            messageToUpdate.isEdited = true;
            await _context.SaveChangesAsync();
        }

        public async Task<List<Message>> GetAll(Guid ownerId)
        {
            return await _context.Messages.Where(b => b.MessageOwnerID == ownerId)?.ToListAsync();
        }

        public async Task<Message> Get(Guid id)
        {
            return await _context.Messages.FirstOrDefaultAsync(x => x.ID == id);
        }

        public async Task<List<Message>> GetMessagesByPage(int page)
        {
            int perPage = 3;
            var count = _context.Messages.CountAsync();
            return await _context.Messages.OrderBy(x => x.DateTime).Skip((page - 1) * perPage).Take(perPage).ToListAsync();
        }
    }
}
