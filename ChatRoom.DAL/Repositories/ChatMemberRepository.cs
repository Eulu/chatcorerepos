﻿using ChatRoom.DAL.Entities;
using ChatRoom.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Repositories
{
    public class ChatMemberRepository : IChatMemberRepository
    {
        private readonly ApplicationDbContext _context;
        public ChatMemberRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Add(ChatMember chatsMember)
        {
            _context.ChatsMembers.Add(chatsMember);
            await _context.SaveChangesAsync();
        }

        public async Task<List<ChatMember>> GetAll()
        {
            return await _context.ChatsMembers.ToListAsync();
        }
    }
}
