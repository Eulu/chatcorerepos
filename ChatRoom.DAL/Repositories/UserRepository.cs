﻿using ChatRoom.DAL.Entities;
using ChatRoom.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;
        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Add(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> Get(Guid id)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.ID == id);
        }

        public async Task<List<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task Remove(Guid id)
        {
            _context.Users.Remove(_context.Users.First(x => x.ID == id));
            await _context.SaveChangesAsync();
        }

        public async Task Update(User user)
        {
            var messageToUpdate = _context.Users.First(b => b.ID == user.ID);

            messageToUpdate.UserName = user.UserName;

            await _context.SaveChangesAsync();
        }
    }
}
