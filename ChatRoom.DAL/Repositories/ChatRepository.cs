﻿using ChatRoom.DAL.Entities;
using ChatRoom.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly ApplicationDbContext _context;
        public ChatRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Add(Chat chat)
        {
            _context.Chats.Add(chat);
            await _context.SaveChangesAsync();
        }

        public async Task<Chat> Get(Guid id)
        {
            return await _context.Chats.FirstOrDefaultAsync(x => x.ID == id);
        }

        public async Task<List<Chat>> GetAll()
        {
            return await _context.Chats.ToListAsync();
        }

        public async Task Remove(Guid id)
        {
            _context.Chats.Remove(_context.Chats.First(x => x.ID == id));
            await _context.SaveChangesAsync();
        }

        public async Task Update(Chat chat)
        {
            var ChatToUpdate = _context.Chats.First(b => b.ID == chat.ID);
            ChatToUpdate.Name = chat.Name;
            await _context.SaveChangesAsync();
        }
    }
}
