﻿using ChatRoom.DAL.Entities;
using ChatRoom.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatRoom.DAL.Repositories
{
    public class DeletedMessageRepository : IDeletedMessageRepository
    {
        private readonly ApplicationDbContext _context;
        public DeletedMessageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Add(DeletedMessage deletedMessage)
        {
            _context.DeletedMessages.Add(deletedMessage);
            await _context.SaveChangesAsync();
        }

        public async Task<List<DeletedMessage>> Get(Guid userId)
        {
            return await _context.DeletedMessages.Where(b => b.UserID == userId)?.ToListAsync();
        }

        public async Task<List<DeletedMessage>> GetAll()
        {
            return await _context.DeletedMessages.ToListAsync();
        }
    }
}
