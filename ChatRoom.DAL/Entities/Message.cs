﻿ using System;
using System.ComponentModel.DataAnnotations;

namespace ChatRoom.DAL.Entities
{
    public class Message
    {
        [Key]
        public Guid ID { get; set; }
        public Guid MessageOwnerID { get; set; }
        public Guid DestinationID { get; set; }
        public Guid ? RepliedMessageID { get; set; }
        public string Text { get; set; }
        public bool isEdited{ get; set; }
        public bool isReplied{ get; set; }
        public DateTime DateTime { get; set; } = DateTime.UtcNow;
    }
}
