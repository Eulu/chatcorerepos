﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatRoom.DAL.Entities
{
    public class ChatMember
    {   
        [Key]
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
    }
}
