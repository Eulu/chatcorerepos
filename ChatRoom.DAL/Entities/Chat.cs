﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatRoom.DAL.Entities
{
    public class Chat
    {
        [Key]
        public Guid ID { get; set; }
        public String Name { get; set; }
    }
}
