﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatRoom.DAL.Entities
{
    public class User
    {
        [Key]
        public Guid ID { get; set; }
        public string UserName { get; set; }
    }
}
