﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatRoom.DAL.Entities
{
    public class DeletedMessage
    {
        [Key]
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public Guid MessageID { get; set; }
    }
}
